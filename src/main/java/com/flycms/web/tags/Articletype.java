package com.flycms.web.tags;

import com.alibaba.fastjson.JSONArray;
import com.flycms.core.base.AbstractTagPlugin;
import com.flycms.core.utils.StringHelperUtils;
import com.flycms.module.article.model.ArticleCategory;
import com.flycms.module.article.model.CategoryTree;
import com.flycms.module.article.service.ArticleCategoryService;
import com.flycms.module.article.utils.CategoryTreeUtil;
import freemarker.core.Environment;
import freemarker.template.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Open source house, All rights reserved
 * 开发公司：28844.com<br/>
 * 版权：开源中国<br/>
 *
 * @author sunkaifei
 * 
 */
@Service
public class Articletype extends AbstractTagPlugin {

	@Autowired
	private ArticleCategoryService articleCategoryService;

	@Override
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void execute(Environment env, Map params, TemplateModel[] loopVars,
			TemplateDirectiveBody body) throws TemplateException, IOException {
		DefaultObjectWrapperBuilder builder = new DefaultObjectWrapperBuilder(Configuration.VERSION_2_3_25);
		// 获取页面的参数
		//指定父级id
		int fatherId = 0;
		
		//处理标签变量
		Map<String, TemplateModel> paramWrap = new HashMap<String, TemplateModel>(params);
		for(String str:paramWrap.keySet()){ 
			if("fatherId".equals(str)){
				if(paramWrap.get(str).toString()==null) {
					fatherId=0;
				}else {
					if(!StringHelperUtils.checkInteger(paramWrap.get(str).toString())) {
						fatherId=0;
					}else {
						fatherId = Integer.parseInt(paramWrap.get(str).toString());
					}
				}
			}
		}
		// 获取文件的分页
		try {
			//JSONArray category = articleCategoryService.getTreeMenu(fatherId);
			//env.setVariable("treeMenu", builder.build().wrap(category));
		} catch (Exception e) {
			env.setVariable("treeMenu", builder.build().wrap(null));
		}
		body.render(env.getOut());
	}
}
